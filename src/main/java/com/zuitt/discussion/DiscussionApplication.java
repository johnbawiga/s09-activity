package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(){
		return "Hello World!";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value="name", defaultValue = "John")String name){
		return String.format("Hi %s",name);
	}

	@GetMapping("/friend")
	public String friend(@RequestParam(value="name",defaultValue = "Joe") String name, @RequestParam(value="friend", defaultValue = "Jane")String friend){
		return String.format("Hello %s! My name is %s.",friend, name);
	}
	// ACTIVITY S09

	// INITIALIZATION
	ArrayList<String> enrollees = new ArrayList<String>();
	//	/enroll
	@GetMapping("/enroll")
	public String enrollUser(@RequestParam(value="user")String user){
		enrollees.add(user);
		return "Thank you for enrolling, "+user+"!";
	}
	// /getEnrollees
	@GetMapping("/getEnrollees")
	public String getEnrollees(){
		return enrollees.toString();
	}
	// nameage
	@GetMapping("/nameage")
	public String nameAge(@RequestParam(value="name")String name,@RequestParam(value="age") int age){
		return "Hello "+name+" My age is "+age;
	}
	// /courses/id
	@GetMapping("/courses/{id}")
	public String getCourse(@PathVariable String id){
		String courseName,schedule,price;
		if(id.equals("java101")){
			courseName = "java101";
			schedule = "MWF 8:00 AM - 11:00 AM";
			price = "PHP 3000";
		}
		else if(id.equals("sql101")){
			courseName = "sql101";
			schedule = "TTH 9:00 AM - 11:00 AM";
			price = "PHP 2500";
		}
		else if(id.equals("javaee101")){
			courseName = "javaee101";
			schedule = "MWF 1:30 PM - 4:00 PM";
			price = "PHP 4000";
		}
		else{
			return "That course cannot be found";
		}
		return "Name: "+ courseName+", "+"Schedule: "+schedule+", "+"Price: "+price;
	}



}
